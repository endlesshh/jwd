package com.luculent;

import org.osgeo.proj4j.CRSFactory;
import org.osgeo.proj4j.CoordinateReferenceSystem;
import org.osgeo.proj4j.CoordinateTransform;
import org.osgeo.proj4j.CoordinateTransformFactory;
import org.osgeo.proj4j.ProjCoordinate;

public class ChangeWp {
	 /**
     * 谷歌下转换经纬度对应的层行列
     *
     * @param lon  经度
     * @param lat  维度
     * @param zoom 在第zoom层进行转换
     * @return
     */
    public static int[] GoogleLonLatToXYZ(double lon, double lat, int zoom) {

        double n = Math.pow(2, zoom);
        double tileX = ((lon + 180) / 360) * n;
        double tileY = (1 - (Math.log(Math.tan(Math.toRadians(lat)) + (1 / Math.cos(Math.toRadians(lat)))) / Math.PI)) / 2 * n;

        int[] xy = new int[2];

        xy[0] = (int) Math.floor(tileX);
        xy[1] = (int) Math.floor(tileY);
        System.out.println(xy[0]+","+xy[1]);
        return xy;
    }

    /**
     * 层行列转经纬度
     *
     * @param x
     * @param y
     * @param z
     * @return
     */
    public static double[] XYZtoLonlat(int z, int x, int y) {

        double n = Math.pow(2, z);
        double lon = x / n * 360.0 - 180.0;
        double lat = Math.atan(Math.sinh(Math.PI * (1 - 2 * y / n)));
        lat = lat * 180.0 / Math.PI;
        double[] lonlat = new double[2];
        lonlat[0] = lon;
        lonlat[1] = lat;
        System.out.println(lonlat[0]+","+lonlat[1]);
        return lonlat;
    }
   //   var e = 1212e4;
   //       ycenter = 406e4,
   //       [(t[1] - ycenter + (t[0] - e)) / Math.sqrt(2) + e, 0.65 * (t[1] - ycenter - (t[0] - e)) / Math.sqrt(2) + ycenter]
   //
    public static double[] etoMercator(double lat,double lng) {
        double e = 12120000;
        double ycenter = 4060000;
        double nlat = (lng - ycenter + (lat - e)) / Math.sqrt(2) + e;
        double nlng = 0.65 * (lng - ycenter - (lat - e)) / Math.sqrt(2) + ycenter;
        System.out.println(nlat+","+nlng);
        return new double[]{nlat, nlng};
    } 
    
    
    public static double[] transform(double lat,double lng, String sourceCRS, String targetCRS) {
       
        CRSFactory factory = new CRSFactory();
        CoordinateReferenceSystem srcCrs = factory.createFromName(sourceCRS); // Use "EPSG:3857" here instead of 900913.
        CoordinateReferenceSystem destCrs = factory.createFromName(targetCRS); // Use "EPSG:4326 here.
        CoordinateTransform transform = new CoordinateTransformFactory().createTransform(srcCrs, destCrs);
        ProjCoordinate srcCoord = new ProjCoordinate(lat, lng);
        ProjCoordinate destCoord = new ProjCoordinate();
        transform.transform(srcCoord, destCoord); 
        System.out.println(destCoord.x+","+destCoord.y);
        return new double[]{destCoord.x, destCoord.y};
        
    }
    
    public static void main(String[] args) throws Exception {
        //13722529.575541047, 4070760.701551185
       /* double[] nn =  etoMercator(13241453.429859893, 5204865.629974255);
        GoogleLonLatToXYZ(nn[0], nn[1],19);

        GoogleLonLatToXYZ(118.950000, 42.2923,19);
        int[] change =  GoogleLonLatToXYZ(123.27158054490693, 34.31190623699369,19);
        System.out.println(change[0] - Math.pow(2, 19 - 1));
        XYZtoLonlat(19,435377,194050);
        transform();*/
    	//transform(118.950000, 42.2923,a,b);
    	transform(118.950000, 42.2923,"EPSG:4326","EPSG:3857");
    	//13241453.429859893, 5204865.629974255 

    }
}